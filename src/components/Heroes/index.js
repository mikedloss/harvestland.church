import { GroupHero } from './GroupHero';
import { ImageHero } from './ImageHero';
import { Side2SideHero } from './Side2SideHero';
import { TextHero } from './TextHero';
import { VideoHero } from './VideoHero';

export const Hero = {
  Group: GroupHero,
  Image: ImageHero,
  Side2Side: Side2SideHero,
  Text: TextHero,
  Video: VideoHero,
};
