// TODO: Put this in contentful
export const leaders = {
  peteHeather: {
    name: 'Pete & Heather Freeman',
    jobs: ['Lead Pastors'],
    email: 'pastors@harvestland.church',
  },
  devanne: {
    name: 'Devanne DLoss',
    jobs: ['Youth Pastor', 'Administrative Pastor'],
    email: 'devanne@harvestland.church',
  },
  mike: {
    name: 'Mike DLoss',
    jobs: ['Creative Director', 'Youth Pastor'],
    email: 'mike@harvestland.church',
  },
  willis: {
    name: 'Willis Greer',
    jobs: ['Worship Leader'],
    email: 'willis@harvestland.church',
  },
  summer: {
    name: 'Summer Posey',
    jobs: ["Children's Director"],
    email: 'summer@harvestland.church',
  },
};
